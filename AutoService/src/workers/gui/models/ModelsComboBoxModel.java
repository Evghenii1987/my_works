package workers.gui.models;

import java.util.List;
import javax.swing.DefaultComboBoxModel;
import workers.entities.Model;

/**
 *
 * @author iucosoft9
 */
public class ModelsComboBoxModel extends DefaultComboBoxModel<Model>{

    public ModelsComboBoxModel(List<Model> lista) {

        Model m = new Model();
        m.setId(0);
        m.setBid(0);
        m.setName("Select.");
        super.addElement(m);
        for (Model model : lista) {
            super.addElement(model);
        }


    }
    
}
