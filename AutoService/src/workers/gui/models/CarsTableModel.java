/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers.gui.models;

import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import workers.entities.Cars;

/**
 *
 * @author iucosoft9
 */
public class CarsTableModel extends DefaultTableModel{
    
    String[] clone = {"idCars","vehicle","model","vin","licensePlate"};
    

     public  CarsTableModel(List<Cars> lista){
         
         for (String col : clone) {
             super.addColumn(col);
         }
         for (Cars cars : lista) {
             
             Vector row = new Vector();
             
             row.addElement(cars.getIdCars());
             row.addElement(cars.getVehicle());
             row.addElement(cars.getModel());
             row.addElement(cars.getVin());
             row.addElement(cars.getLicensePlate());
             
             super.dataVector.add(row);
         }
    
    
}    
}
