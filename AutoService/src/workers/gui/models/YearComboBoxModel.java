package workers.gui.models;

import java.time.LocalDate;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import workers.entities.Model;

/**
 *
 * @author iucosoft9
 */
public class YearComboBoxModel  extends DefaultComboBoxModel<Integer>{

    public YearComboBoxModel() {
        LocalDate d=  LocalDate.now();

        int currentYear=d.getYear();
        System.out.println("wwwwwwwwwwwww="+currentYear);
        for (int i = 1980; i <=currentYear; i++) {
            
              super.addElement(i);
        }
      

    }
    
}
