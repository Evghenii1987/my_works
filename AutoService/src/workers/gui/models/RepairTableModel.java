package workers.gui.models;

import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import workers.entities.Repair;

/**
 *
 * @author User
 */
public class RepairTableModel extends DefaultTableModel{
    
    String[] colone= {"idRepair","idCars","idProblems","idWorker","completedWork","payment"};
    
    
    
    
    public RepairTableModel(List<Repair> liste){
        
        for(String col: colone){
            super.addColumn(col);
        }
        for(Repair cr: liste){
            
            Vector row = new Vector();
            
            row.addElement(cr.getIdRepair());
            row.addElement(cr.getIdCars());
            row.addElement(cr.getIdProblems());
            row.addElement(cr.getIdWorker());
            row.addElement(cr.getCompletedWork());
            row.addElement(cr.getPayment());
            
        }
    }
}
