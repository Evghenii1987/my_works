package workers.gui.models;

import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import workers.entities.Repair;
import workers.entities.Clients;

/**
 *
 * @author User
 */
public class ClientsTableModel extends DefaultTableModel{
    
    String[] colone = {"IdClient","Name","PhoneNumber"};

    
    public ClientsTableModel( List<Clients> lista){
        
        for (String col: colone){
            super.addColumn(col);
        }
        for (Clients cl: lista){
            
            Vector row = new Vector();
            row.addElement(cl.getIdClient()); 
            row.addElement(cl.getName()); 
            row.addElement(cl.getPhoneNumber()); 
                              
              super.dataVector.add(row);
        }
      
            
        }
        
 
        
           
}
