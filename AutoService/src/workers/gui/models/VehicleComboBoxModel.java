package workers.gui.models;

import java.awt.image.DirectColorModel;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import workers.dao.impl.ClientsDaoImp;
import workers.db.DataSourse;
import workers.entities.Brand;


/**
 *
 * @author iucosoft9
 */
public class VehicleComboBoxModel extends DefaultComboBoxModel<Brand>{

    public VehicleComboBoxModel(List<Brand> lista) {
        Brand br = new Brand();
        br.setId(0);
        br.setName("Select.");
        super.addElement(br);
        for (Brand brand : lista) {
            super.addElement(brand);
        }
    }
 
    
    
}

