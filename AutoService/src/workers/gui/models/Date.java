package workers.gui.models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;

/**
 *
 * @author User
 */
public class Date  {
    
 
//    jLabelDate labelDate;   
//    jPanel4 mainPanel;
//    
// 
//    jLabelDate jl=new JLabel("Date:");
    JFrame jf=new JFrame("Test SpinnerDateFormat");

    JButton jb=new JButton("Get Date");
    SpinnerDateModel spinMod=new SpinnerDateModel();
    JSpinner spin=new JSpinner(spinMod);

    class jbHandler implements ActionListener{
        public void actionPerformed(ActionEvent evt){
            JSpinner.DateEditor de=(JSpinner.DateEditor)spin.getEditor();
            String wrongDate=de.getFormat().format(spin.getValue());
            Date okDate=(Date)spin.getValue();
            System.out.println(">>>\n"+wrongDate+"\n"+okDate);
        }
    }

    JPanel pane=new JPanel();

    public Date(){

        jf.setLocationRelativeTo(null);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pane.add(jf);

        jb.addActionListener(new jbHandler());
        pane.add(jb);

        spin.setValue(new Date());//this doesn't work properly with the custom format
        spin.setEditor(new JSpinner.DateEditor(spin,"dd.mm.yyyy"));
        pane.add(spin);

        jf.add(pane);
        jf.pack();
        jf.setVisible(true);
    }

    public static void main(String[] args){

    SwingUtilities.invokeLater(new Runnable(){
        public void run(){
            Date tdate=new Date();
        }
        });
    }
}
    
