package workers.entities;

import java.util.Objects;

/**
 *
 * @author iucosoft9
 */
public class Model {
    private int id;
    private int bid;
    private String name;

    public Model() {
    }

    public Model(int id, int bid, String name) {
        this.id = id;
        this.bid = bid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.bid;
        hash = 67 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Model other = (Model) obj;
        if (this.bid != other.bid) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  name ;
    }
    
    
    
}
