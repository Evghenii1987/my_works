package workers.entities;

/**
 *
 * @author User
 */
public class Years {

private  int years;

    public Years(int years) {
        this.years = years;
    }

    public Years() {
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.years;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Years other = (Years) obj;
        if (this.years != other.years) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Years{" + "years=" + years + '}';
    }

}
