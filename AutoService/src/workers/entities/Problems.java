package workers.entities;

import java.util.Objects;

/**
 *
 * @author User
 */
public class Problems {
    
    private int idProblems;
    private String descriptionProblem;
    private int idWorker;
    private int idRepair;

    public Problems() {
    }

    public Problems(int idProblems, String descriptionProblem, int idWorker, int idRepair) {
        this.idProblems = idProblems;
        this.descriptionProblem = descriptionProblem;
        this.idWorker = idWorker;
        this.idRepair = idRepair;
    }

    public int getIdProblems() {
        return idProblems;
    }

    public void setIdProblems(int idProblems) {
        this.idProblems = idProblems;
    }

    public String getDescriptionProblem() {
        return descriptionProblem;
    }

    public void setDescriptionProblem(String descriptionProblem) {
        this.descriptionProblem = descriptionProblem;
    }

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public int getIdRepair() {
        return idRepair;
    }

    public void setIdRepair(int idRepair) {
        this.idRepair = idRepair;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.idProblems;
        hash = 79 * hash + this.idWorker;
        hash = 79 * hash + this.idRepair;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Problems other = (Problems) obj;
        if (this.idProblems != other.idProblems) {
            return false;
        }
        if (this.idWorker != other.idWorker) {
            return false;
        }
        if (this.idRepair != other.idRepair) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Problems{" + "idProblems=" + idProblems + ", descriptionProblem=" + descriptionProblem + ", idWorker=" + idWorker + ", idRepair=" + idRepair + '}';
    }
    

    
}
