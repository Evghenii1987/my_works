package workers.entities;

import java.util.Objects;

/**
 *
 * @author User
 */
public class Cars {

    private int idCars;
    private String vehicle;
    private String model;
    private int year;
    private String vin;
    private String licensePlate;
    private String motor;

    public Cars() {
    }

    public Cars(String vehicle, String model, int year, String vin, String licensePlate, String motor) {
        this.vehicle = vehicle;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.licensePlate = licensePlate;
        this.motor = motor;
    }

    public int getIdCars() {
        return idCars;
    }

    public void setIdCars(int idCars) {
        this.idCars = idCars;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.idCars;
        hash = 97 * hash + Objects.hashCode(this.vin);
        hash = 97 * hash + Objects.hashCode(this.licensePlate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cars other = (Cars) obj;
        if (this.idCars != other.idCars) {
            return false;
        }
        if (!Objects.equals(this.vin, other.vin)) {
            return false;
        }
        if (!Objects.equals(this.licensePlate, other.licensePlate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cars{" + "idCars=" + idCars + ", vehicle=" + vehicle + ", model=" + model + ", year=" + year + ", vin=" + vin + ", licensePlate=" + licensePlate + ", motor=" + motor + '}';
    }
 
}

