package workers.entities;

import java.util.Objects;

/**
 *
 * @author User
 */
public class Repair {
  
    private int idRepair;
    private int idCars;
    private int idProblems;
    private int idWorker;
    private String completedWork;
    private int payment;

    public Repair() {
    }

    public Repair(int idRepair, int idCars, int idProblems, int idWorker, String completedWork, int payment) {
        this.idRepair = idRepair;
        this.idCars = idCars;
        this.idProblems = idProblems;
        this.idWorker = idWorker;
        this.completedWork = completedWork;
        this.payment = payment;
    }

    public int getIdRepair() {
        return idRepair;
    }

    public void setIdRepair(int idRepair) {
        this.idRepair = idRepair;
    }

    public int getIdCars() {
        return idCars;
    }

    public void setIdCars(int idCars) {
        this.idCars = idCars;
    }

    public int getIdProblems() {
        return idProblems;
    }

    public void setIdProblems(int idProblems) {
        this.idProblems = idProblems;
    }

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public String getCompletedWork() {
        return completedWork;
    }

    public void setCompletedWork(String completedWork) {
        this.completedWork = completedWork;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.idRepair;
        hash = 97 * hash + this.idCars;
        hash = 97 * hash + this.idProblems;
        hash = 97 * hash + this.idWorker;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Repair other = (Repair) obj;
        if (this.idRepair != other.idRepair) {
            return false;
        }
        if (this.idProblems != other.idProblems) {
            return false;
        }
        if (this.idWorker != other.idWorker) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Repair{" + "idRepair=" + idRepair + ", idCars=" + idCars + ", idProblems=" + idProblems + ", idWorker=" + idWorker + ", completedWork=" + completedWork + ", payment=" + payment + '}';
    }
    

}