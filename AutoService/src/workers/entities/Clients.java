package workers.entities;

import java.util.Objects;

/**
 *
 * @author User
 */
public class Clients {

    private int idClient;
    private String name;
    private int phoneNumber;

    public Clients() {
    }
  public Clients(String name, int phoneNumber) {
        
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
    public Clients(int idClient, String name, int phoneNumber) {
        this.idClient = idClient;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.idClient;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + this.phoneNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clients other = (Clients) obj;
        if (this.idClient != other.idClient) {
            return false;
        }
        if (this.phoneNumber != other.phoneNumber) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Clients{" + "idClient=" + idClient + ", name=" + name + ", phoneNumber=" + phoneNumber + '}';
    }
    
}
