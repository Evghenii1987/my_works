package workers.fileservicii;

import java.util.Collection;

/**
 *
 * @author iucosoft9
 */
public interface Readable<T> {
      Collection<T> read ( String fileSrc );
}
