package workers.fileservicii;

import java.util.Collection;

/**
 *
 * @author iucosoft9
 */
public interface Writable<T> {
    void write(Collection<T> coll, String fileDest);
}
