package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Clients;

/**
 *
 * @author User
 */
public interface ClientsDaoIntf {

  
    void save (Clients cl) throws SQLException;
    void update (Clients cl) throws SQLException;
    void delete (Clients cl) throws SQLException;

    Clients findByClientId (int id) throws SQLException;
    List<Clients> findAll () throws SQLException;

    public Clients findByName(String name)throws SQLException;
    
}
