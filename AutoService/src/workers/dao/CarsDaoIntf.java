package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Cars;
import workers.entities.Workers;

/**
 *
 * @author User
 */
public interface CarsDaoIntf {
    
    void save (Cars cs)throws SQLException;
    void update (Cars cs)throws SQLException;
    void delete (Cars cs)throws SQLException;
    
    Cars findById (int id) throws SQLException;
    Cars findByVin (String vin) throws SQLException;
    List<Cars> findAll() throws SQLException;
}
