package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Problems;
import workers.entities.Workers;

/**
 *
 * @author User
 */
public interface ProblemsdaoIntf {

   void save (Problems wp) throws SQLException;
   void update (Problems wp) throws SQLException;
   void delete (Problems wp) throws SQLException;
    
   void findByIdProblems (int id)throws SQLException;
   List<Problems> findAll() throws SQLException;
  
   
}
