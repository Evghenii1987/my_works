package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Model;

/**
 *
 * @author User
 */
public interface ModelDaoIntf {
    
   List <Model> findByBid (int bid)throws SQLException;
    Model findByName (String name)throws SQLException;
    List<Model> findAll()throws SQLException;

    
  
    
    
}
