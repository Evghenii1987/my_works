package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Brand;

/**
 *
 * @author iucosoft9
 */
public interface BrandDaoIntf {

   
    Brand findByName (String name)throws SQLException;
     List<Brand> findAll ()throws SQLException;
    
}
