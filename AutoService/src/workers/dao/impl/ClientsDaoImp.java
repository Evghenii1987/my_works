package workers.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.dao.ClientsDaoIntf;
import workers.db.DataSourse;
import workers.entities.Clients;

/**
 *
 * @author User
 */
public class ClientsDaoImp implements ClientsDaoIntf {

    private static final Logger LOG = Logger.getLogger(ClientsDaoImp.class.getName());
    private DataSourse ds = DataSourse.getInstatnce();

    private Connection connection;

    @Override
    public void save(Clients cl) throws SQLException {
        
        String sql = "INSERT INTO clients (NAME, NUMBER_PHONE)"
                + " VALUES(?,?);";
               

        connection = ds.getConnection();
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, cl.getName());
        st.setInt(2, cl.getPhoneNumber());
        st.executeUpdate();
        LOG.info("Save was done!");
        connection.close();

    }

    @Override
    public void update(Clients cl) throws SQLException {

        String sql = "UPDATE clients SET WHERE NAME =" + cl.getName();

        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Update was done!");
        connection.close();

    }

    @Override
    public void delete(Clients cl) throws SQLException {

        String sql = "DELETE* FROM clients WHERE ID_CLIENT=" + cl.getIdClient();

        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Delete was done!");

        connection.close();
    }

   @Override
    public List<Clients> findAll() throws SQLException {

        List<Clients> lista = new ArrayList<>();
        String sql = "SELECT * FROM Clients ";
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {

            Clients cl = new Clients(rs.getInt(1), rs.getString(2), rs.getInt(3));
            lista.add(cl);
        }

        connection.close();
        return lista;

    }

    @Override
    public Clients findByName(String name) throws SQLException {
        Clients cl = null;

        String sql = "SELECT * FROM clients WHERE NAME="+cl.getName();
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        if (rs.next()) {
           
            cl = new Clients(rs.getInt(1), rs.getString(2), rs.getInt(3));
            System.out.println("DaoCl = " + cl);
        }

        connection.close();
        return cl;

    }

    @Override
    public Clients findByClientId(int id) throws SQLException {
        Clients cl = null;

        String sql = "SELECT * FROM clients" + id;
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        if (rs.next()) {

            cl = new Clients(rs.getInt(1), rs.getString(2), rs.getInt(3));
        
    }
 connection.close();
        return cl;
}
}
