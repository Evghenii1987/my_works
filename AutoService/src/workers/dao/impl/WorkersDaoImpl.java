package workers.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.dao.WorkersDaoIntf;
import workers.db.DataSourse;
import workers.entities.Workers;

/**
 *
 * @author User
 */
public class WorkersDaoImpl implements WorkersDaoIntf{
    
    private static final Logger LOG= Logger.getLogger(WorkersDaoImpl.class.getName());
    private  DataSourse ds = DataSourse.getInstatnce();

    private Connection connection;
    
    

    @Override
    public void save(Workers wk) throws SQLException {
        
        String sql = "INSERT INTO workers (`ID_WORKER`, `WORKER_ NAME`, `PAY_OFF`, `SALARY`)"
                + " VALUES"+wk.getId()+wk.getName()+wk.getPayoff()+wk.getSalary();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Save OK!");
        connection.close();
    }

    @Override
    public void updete(Workers wk) throws SQLException {
        
        String sql = "UPDATE workers WHERE ID_WORKER="+wk.getId();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Update OK!");
        connection.close();


    }

    @Override
    public void delete(Workers wk) throws SQLException {
        
        String sql = "DELETE * FROM workers WHERE ID_WORKER="+wk.getId();
        
         connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Delete is OK!");
        connection.close();


    }

    @Override
    public Workers findById(int id) throws SQLException {
        Workers wk = null;
        
        String sql = "SELECT* FROM workers WHERE  ID_WORKER="+id;
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            
            wk = new Workers(rs.getInt(1),rs.getString(2), rs.getDouble(3), rs.getDouble(4));
                       
        }
        
        connection.close();
        return wk;


    }

    @Override
    public List<Workers> findAll() throws SQLException {
                
         List<Workers> lista = new ArrayList<>();
        String sql = "SELECT* FROM workers";
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            
           Workers wk = new Workers(rs.getInt(1),rs.getString(2), rs.getDouble(3), rs.getDouble(4));
                       
        }
        
        connection.close();
        return lista;
   
    }
    
}
