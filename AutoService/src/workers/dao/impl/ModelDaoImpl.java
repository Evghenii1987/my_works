package workers.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.dao.ModelDaoIntf;
import workers.db.DataSourse;
import workers.entities.Model;

/**
 *
 * @author User
 */
public class ModelDaoImpl implements ModelDaoIntf {

    private static final Logger LOG = Logger.getLogger(ModelDaoImpl.class.getName());
    private DataSourse ds = DataSourse.getInstatnce();

    private Connection connection;

    @Override
    public Model findByName(String name) throws SQLException {

        Model m = null;
        connection = ds.getConnection();
        String sql = "SELECT * FROM  model WHERE name='" + name + "'";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        if (rs.next()) {
            m = new Model(rs.getInt(1), rs.getInt(2), rs.getString(3));
        }
        connection.close();
        return m;
    }

    @Override
    public List<Model> findAll() throws SQLException {

        List<Model> lista = new ArrayList<>();
        connection = ds.getConnection();

        String sql = "SELECT * FROM  model";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {
            Model m = new Model(rs.getInt(1), rs.getInt(2), rs.getString(3));
            lista.add(m);
        }
        connection.close();
        return lista;

    }

    @Override
    public List<Model> findByBid(int bid) throws SQLException {
        List<Model> lista = new ArrayList<>();
        connection = ds.getConnection();
        String sql = "SELECT * FROM  model WHERE bid="+bid;
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {
            Model m = new Model(rs.getInt(1), rs.getInt(2), rs.getString(3));
            lista.add(m);
        }
        connection.close();
        return lista;

    }

}
