package workers.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.db.DataSourse;
import workers.entities.Problems;
import workers.dao.ProblemsdaoIntf;

/**
 *
 * @author User
 */
 public class ProblemsDaoImpl implements ProblemsdaoIntf{
     
     private static final Logger LOG = Logger.getLogger(ProblemsDaoImpl.class.getName());
     private DataSourse ds = DataSourse.getInstatnce();
     
     private Connection connection;

    @Override
    public void save(Problems pr) throws SQLException {

        String sql = "INSERT INTO problems (`ID_PROBLEMS`, `DESCRIPTION_PROBLEM`, `ID_WORKER`, `ID_REPAIR`)"
                + " VALUES"+pr.getIdProblems()+pr.getDescriptionProblem()+pr.getIdWorker()
                +pr.getIdRepair();
        
        
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Save done!");
        
        connection.close();


    }

    @Override
    public void update(Problems pr) throws SQLException {

        String sql = "UPDATE problems SET WHERE ID_PROBLEMS="+pr.getIdProblems();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Update done!");
        
        connection.close();


    }

    @Override
    public void delete(Problems pr) throws SQLException {

        String sql = "DELET* FROM problems WHERE ID_PROBLEMS="+pr.getIdProblems();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Delete done!");

        connection.close();

    }


    @Override
    public void findByIdProblems(int id) throws SQLException {
        Problems pr= null;
        
        String sql = "SELECT *FROM problems WHERE ID_PROBLEMS="+pr.getIdProblems();
        
        connection=ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            
            pr = new Problems(rs.getInt(1),rs.getString(2),rs.getInt(3),
            rs.getInt(4));
        }

        connection.close();
        
    }

    @Override
    public List<Problems> findAll() throws SQLException {
       List<Problems>  lista = new ArrayList<>();
        String sql ="SELECT* FROM problems";
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            Problems pr = new Problems(rs.getInt(1),rs.getString(2),rs.getInt(3),
                                       rs.getInt(4));
        }
        connection.close();
         return lista;
        
        
    }

   
}
