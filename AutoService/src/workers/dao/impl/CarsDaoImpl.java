package workers.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.dao.CarsDaoIntf;
import workers.db.DataSourse;
import workers.entities.Cars;
import workers.entities.Workers;

/**
 *
 * @author User
 */
public class CarsDaoImpl implements CarsDaoIntf {

    private static final Logger LOG = Logger.getLogger(CarsDaoImpl.class.getName());
    private DataSourse ds = DataSourse.getInstatnce();

    private Connection connection;

    @Override
    public void save(Cars cs) throws SQLException {

        String sql = "INSERT INTO cars (VEHICLE,MODEL,YEAR,VIN,LICENSE_PLATE,MOTOR)"
                
                + " VALUES(?,?,?,?,?,?);";
                
               

        connection = ds.getConnection();
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, cs.getVehicle());
        st.setString(2, cs.getModel());
        st.setInt(3, cs.getYear());
        st.setString(4, cs.getVin());
        st.setString(5, cs.getLicensePlate());
        st.setString(6, cs.getMotor());
        
        st.executeUpdate();
        LOG.info("Save OK!");
        connection.close();

    }

    @Override
    public void update(Cars cs) throws SQLException {

        String sql = "UPDATE cars SET  WHERE ID_CARS =" + cs.getIdCars();

        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Update OK!");
        connection.close();

    }

    @Override
    public void delete(Cars cs) throws SQLException {

        String sql = "DELETE * FROM cars WHERE ID_CARS" + cs.getIdCars();

        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Delete OK!");
        connection.close();

    }

    @Override
    public  Cars findById(int id) throws SQLException {
        Cars cs = null;

        String sql = "SELECT* FROM cars WHERE ID_CARS=" + id;
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        ResultSet rs = st.executeQuery(sql);

        if (rs.next()) {
//            cs = new Cars(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getString(5),
//            rs.getString(6),rs.getString(7));
        }

        connection.close();
        return cs;

    }

 

    @Override
    public List<Cars> findAll() throws SQLException {

        List<Cars> lista = new ArrayList<>();

        String sql = "SELECT* FROM cars ";
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {

//            Cars cs = new Cars(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));

//            lista.add(cs);
        }

        connection.close();
        return lista;

    }

    @Override
    public Cars findByVin(String vin) throws SQLException {
        
        Cars cs = null;

        String sql = "SELECT* FROM cars WHERE VIN=" + vin;
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        ResultSet rs = st.executeQuery(sql);

        if (rs.next()) {
//            cs = new Cars(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
//                    rs.getString(5));
        }

        connection.close();
        return cs;

        
    }


}
