/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.db.DataSourse;
import workers.entities.Repair;
import workers.dao.RepairDaoIntf;

/**
 *
 * @author User
 */
public class RepairDaoImpl implements RepairDaoIntf{
    
    private static final Logger LOG = Logger.getLogger(RepairDaoImpl.class.getName());
    private  DataSourse ds= DataSourse.getInstatnce();
    
    private Connection connection;
    @Override
    public void save(Repair cr) throws SQLException {

        String sql = "INSERT INTO repair (`ID_REPAIR`, `ID_CARS`,"
                + " `ID_PROBLEMS`, `ID_WORKER`, `COMPLETED_WORK`, `PAYMENT`) VALUES "
                + cr.getIdRepair()+cr.getIdCars()+cr.getIdProblems()+cr.getIdWorker()
                + cr.getCompletedWork()+cr.getPayment();
                
        connection= ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Saving is done well!");
        connection.close();
    }

    @Override
    public void update(Repair cr) throws SQLException {

        String sql = "UPDATE repair SET WHERE ID_REPAIR='"+cr.getIdRepair();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Updating is done well!");
        connection.close();

    }

    @Override
    public void delete(Repair cr) throws SQLException {

        String sql = "DELETE * FROM repair WHERE ID_REPAIR="+cr.getIdRepair();
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        st.executeUpdate(sql);
        LOG.info("Delete is done well!");
        connection.close();

    }

    @Override
    public void findByIdRepair(int id) throws SQLException {

        Repair cr= null;
        String sql = "SELECT*FROM repair WHERE ID_REPAIR="+cr.getIdRepair();
        
        connection= ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            cr = new Repair(rs.getInt(1),rs.getInt(2),rs.getInt(3),
            rs.getInt(4),rs.getString(5),rs.getInt(6));
        }

        connection.close();
    }

    @Override
    public List<Repair> findAll() throws SQLException {
 
        List<Repair> lista = new ArrayList<>();
        
        String sql = "SELECT* FROM repair";
        
        connection = ds.getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if(rs.next()){
            Repair cr = new Repair (rs.getInt(1),rs.getInt(2),rs.getInt(3),
            rs.getInt(4),rs.getString(5),rs.getInt(6));
        }
        connection.close();
        return lista;
        
       
    }

}
