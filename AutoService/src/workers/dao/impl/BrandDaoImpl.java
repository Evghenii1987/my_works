package workers.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import workers.dao.BrandDaoIntf;
import workers.db.DataSourse;
import workers.entities.Brand;

/**
 *
 * @author iucosoft9
 */
public class BrandDaoImpl implements BrandDaoIntf{
    
    private static final Logger LOG = Logger.getLogger(BrandDaoImpl.class.getName());
    private DataSourse ds = DataSourse.getInstatnce();
    
    private Connection connection;

    

   
    @Override
    public List<Brand> findAll() throws SQLException {

        List<Brand> lista = new ArrayList<>();
        connection = ds.getConnection();
        
        String sql = "SELECT * FROM  brand"; 
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
       while (rs.next()) {
         Brand   br = new Brand (rs.getInt(1), rs.getString(2));
         lista.add(br);
        }

        connection.close();
        return lista;
    }

    @Override
    public Brand findByName(String name) throws SQLException {
             Brand br = null;
        String sql = "SELECT * FROM brand WHERE name ='"+name+"'";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            br = new Brand(rs.getInt(1),rs.getString(2));
            
        }
        connection.close();
        return br;
    }
    
}
