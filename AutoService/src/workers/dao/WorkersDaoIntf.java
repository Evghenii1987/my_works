package workers.dao;

import java.sql.SQLException;
import java.util.List;
import workers.entities.Workers;

/**
 *
 * @author User
 */
public interface WorkersDaoIntf {
    
    void save(Workers wk)throws SQLException;
    void updete(Workers wk)throws SQLException;
    void delete(Workers wk)throws SQLException;
    
    Workers findById (int id) throws SQLException;
    
    List<Workers> findAll() throws SQLException;
}
