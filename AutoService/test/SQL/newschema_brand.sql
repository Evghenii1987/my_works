-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: newschema
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Microcar'),(2,'AC'),(3,'Acura'),(4,'Admiral'),(5,'Alfa Romeo'),(6,'Alpina'),(7,'Aro'),(8,'Asia'),(9,'Aston Martin'),(10,'Audi'),(11,'BMW'),(12,'BYD'),(13,'Bentley'),(14,'Brilliance'),(15,'Bugatti'),(16,'Buick'),(17,'Cadillac'),(18,'Caterham'),(19,'ChangFeng'),(20,'Chery'),(21,'Chevrolet'),(22,'Chrysler'),(23,'Citroen'),(24,'DADI'),(25,'Dacia'),(26,'Daewoo'),(27,'Daihatsu'),(28,'Daimler'),(29,'De Tomaso'),(30,'Derways'),(31,'Dodge'),(32,'Doninvest'),(33,'Donkervoort'),(34,'Eagle'),(35,'FAW'),(36,'FSO'),(37,'FSR'),(38,'Ferrari'),(39,'Fiat'),(40,'Ford'),(41,'GMC'),(42,'Geely'),(43,'Geo'),(44,'Ginetta'),(45,'Gonow'),(46,'Great Wall'),(47,'Hafei'),(48,'Holden'),(49,'Honda'),(50,'Huanghai'),(51,'Hummer'),(52,'Hyundai'),(53,'Infiniti'),(54,'Intrall'),(55,'Iran Khodro'),(56,'Isuzu'),(57,'JMC'),(58,'Jaguar'),(59,'Jeep'),(60,'Jindei'),(61,'Kia'),(62,'Lamborghini'),(63,'Lancia'),(64,'Land Rover'),(65,'Landwind'),(66,'Lexus'),(67,'Lifan'),(68,'Lincoln'),(69,'Lotus'),(70,'MG'),(71,'MINI'),(72,'Mahindra'),(73,'Marcos'),(74,'Maruti'),(75,'Maserati'),(76,'Maybach'),(77,'Mazda'),(78,'McLaren'),(79,'Mercedes-Benz'),(80,'Mercury'),(81,'Metrocab'),(82,'Mitsubishi'),(83,'Mitsuoka'),(84,'Morgan'),(85,'Nissan'),(86,'Oldsmobile'),(87,'Opel'),(88,'PUCH'),(89,'Pagani'),(90,'Panoz'),(91,'Peugeot'),(92,'Plymouth'),(93,'Pontiac'),(94,'Porsche'),(95,'Proton'),(96,'Renault'),(97,'Roewe'),(98,'Rolls-Royce'),(99,'Rover'),(100,'SEAT'),(101,'Saab'),(102,'Saleen'),(103,'Samsung'),(104,'Saturn'),(105,'Scion'),(106,'Shenlong'),(107,'Shuanghuan'),(108,'Skoda'),(109,'Smart'),(110,'Spyker'),(111,'SsangYong'),(112,'Subaru'),(113,'Suzuki'),(114,'TVR'),(115,'Talbot'),(116,'Tata'),(117,'Tatra'),(118,'Tianma'),(119,'Tianye'),(120,'Tofas'),(121,'Toyota'),(122,'Trabant'),(123,'Venturi'),(124,'Volkswagen'),(125,'Volvo'),(126,'Vortex'),(127,'Wartburg'),(128,'Wiesmann'),(129,'Xin Kai'),(130,'Yzk'),(131,'ZX'),(132,'ВАЗ'),(133,'Велта'),(134,'ГАЗ'),(135,'ЕРАЗ'),(136,'СеАЗ'),(137,'КАМАЗ'),(138,'ЗАЗ'),(139,'ЗИЛ'),(140,'ИЖ'),(141,'ЛУАЗ'),(142,'Москвич'),(143,'УАЗ');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-25 13:54:40
