package sample;

import sample.dao.User;

import java.sql.*;

public class DBSource extends Config {

    Connection connection;

    public Connection getConection() throws ClassNotFoundException,SQLException{
        String connectionString= "jdbc:mysql://"+dbHost+":"+dbPort+"/"+dbName;

        Class.forName("com.mysql.cj.jdbc.Driver");

        connection= DriverManager.getConnection(connectionString,dbUser,dbPassword);

        return connection;
    }

//    Put user in DB
    public void singUpUser(User user){
        String insert = "INSERT INTO "+Const.USER_TABLE+"("+
                Const.USER_FIRSTNAME+","+Const.USER_LASTNAME+","+
                Const.USER_LOGIN+","+Const.USER_PASSWORD+","+
                Const.USER_LOCATION+","+Const.USER_GENDER+")"+
                "VALUES (?,?,?,?,?,?)";


        try {
            PreparedStatement pS= getConection().prepareStatement(insert);
            pS.setString(1, user.getFirstName());
            pS.setString(2, user.getLastName());
            pS.setString(3, user.getLogin());
            pS.setString(4, user.getPassword());
            pS.setString(5, user.getLocation());
            pS.setString(6, user.getGender());

            pS.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public ResultSet getUser(User user){
        ResultSet resSet= null;

        String select = "SELECT * FROM "+ Const.USER_TABLE+ " WHERE "+
                Const.USER_LOGIN+" =? AND "+Const.USER_PASSWORD+" =?";

        try {
            PreparedStatement pS= getConection().prepareStatement(select);

            pS.setString(1, user.getLogin());
            pS.setString(2, user.getPassword());


            resSet=pS.executeQuery(); // Take from DB

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return resSet;

    }
}
