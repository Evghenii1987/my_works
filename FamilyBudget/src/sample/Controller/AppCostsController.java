package sample.Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AppCostsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField Cost1;

    @FXML
    private TextField Cost2;

    @FXML
    private TextField wasCost1;

    @FXML
    private TextField wasCost2;

    @FXML
    private Button AddNewCostButton;

    @FXML
    private TextField TotalCosts;

    @FXML
    private Button CalculateCostsButton;

    @FXML
    private Button NextToPlanButton;

    @FXML
    private TextField actualCost1;

    @FXML
    private TextField actualCost2;

    @FXML
    private TextField Cost3;

    @FXML
    private TextField Cost4;

    @FXML
    private TextField wasCost3;

    @FXML
    private TextField wasCost4;

    @FXML
    private TextField actualCost3;

    @FXML
    private TextField actualCost4;

    @FXML
    private TextField Cost5;

    @FXML
    private TextField Cost6;

    @FXML
    private TextField wasCost5;

    @FXML
    private TextField wasCost6;

    @FXML
    private TextField actualCost5;

    @FXML
    private TextField actualCost6;

    @FXML
    private TextField Cost7;

    @FXML
    private TextField wasCost7;

    @FXML
    private TextField actualCost7;

    @FXML
    void initialize() {


    }
}
