package sample.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DBSource;
import sample.dao.User;

public class SignUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button RegistrationButton;

    @FXML
    private PasswordField SignUpPasswordField;

    @FXML
    private TextField SignUpFirstName;

    @FXML
    private TextField SignUpLastName;

    @FXML
    private TextField SignUpLoginField;

    @FXML
    private CheckBox SignUpCheckBoxMen;

    @FXML
    private CheckBox SignUpCheckBoxWomen;

    @FXML
    private TextField SignUpCountry;

    @FXML
    void initialize() {



                RegistrationButton.setOnAction(event -> {
                   openWorkSpace("/sample/fxmlFiles/sample.fxml");

                    signUpNewUser();




                });

    }

    private void openWorkSpace(String open) {
        RegistrationButton.getScene().getWindow().hide();

        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource(open));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root =loader.getRoot();
        Stage s = new Stage();
        s.setScene(new Scene(root));
        s.showAndWait();
    }

    private void signUpNewUser() {
        DBSource dbSource=new DBSource();

        String firstName = SignUpFirstName.getText();
        String lastName = SignUpLastName.getText();
        String login = SignUpLoginField.getText();
        String password = SignUpPasswordField.getText();
        String location = SignUpCountry.getText();
        String gender = "";


        if(SignUpCheckBoxMen.isSelected()){
            gender= "Men";
        }else{
            gender="Women";
        }


        User user =new User(firstName, lastName, login, password, location, gender);



        dbSource.singUpUser(user);


    }

}
