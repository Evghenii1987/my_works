package sample.Controller;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import animation.Shake;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DBSource;
import sample.dao.User;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField LoginField;

    @FXML
    private PasswordField PasswordField;

    @FXML
    private Button EnterButton;

    @FXML
    private Button RregistrationButton;

    @FXML
    void initialize() {
        //        SingUp, Enter....
        EnterButton.setOnAction(event -> {
            String loginText = LoginField.getText().trim();
            String password = PasswordField.getText().trim();

            if (!loginText.equals("")&&!password.equals("")){
                loginUser(loginText,password);
            }else{
                System.out.println("Login or password is Empty!");
            }

        });


//        Registration
        RregistrationButton.setOnAction(event -> {
            openWorkSpace("/sample/fxmlFiles/signUp.fxml");
        });


    }

    private void loginUser(String loginText, String password) {
        DBSource dbSource=new DBSource();
        User user= new User();
        user.setLogin(loginText);
        user.setPassword(password);
        ResultSet resultSet = dbSource.getUser(user) ;

        int counter=0;

        while (true){
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            counter++;
        }
        if (counter>=1){
            openWorkSpace("/sample/fxmlFiles/appIncomes.fxml");

        }else{
            Shake userLogin= new Shake(LoginField);
            Shake userPassw= new Shake(PasswordField);

            userLogin.playAnimation();
            userPassw.playAnimation();
        }


    }

    public void openWorkSpace(String open){
        RregistrationButton.getScene().getWindow().hide();

        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource(open));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root= loader.getRoot();
        Stage stage= new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
}

