package sample.Controller;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AppIncomesController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField Income1;

    @FXML
    private TextField Income2;

    @FXML
    private TextField EnterIncome1;

    @FXML
    private TextField EnterIncome2;

    @FXML
    private Button AddNewIncomesButton;

    @FXML
    private TextField TotalIncomes;

    @FXML
    private Button CalculateIncomesButton;

    @FXML
    private Button NextButton;

    @FXML
    void initialize() {
//        Next, go to the Costs page,
            NextButton.setOnAction(event -> {

                openWorkSpace("/sample/fxmlFiles/appCosts.fxml");

            });

    }
    public void openWorkSpace(String open){
        NextButton.getScene().getWindow().hide();

        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource(open));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root= loader.getRoot();
        Stage stage= new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
}